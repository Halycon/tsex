let BaseTemplate = document.createElement('template')
BaseTemplate.innerHTML = `
    <style>
    :host {
    margin: 20px;
    }
    .chat-window {
    height: 300px;
    width: 250px;
    margin: 20px;
    background: ghostwhite;
    overflow-wrap: break-word;
    overflow-y: scroll;
    }
    textarea {
        display: block;
        margin: 20px;
        resize: none;
   
    }
</style>
    
    <div>
        <div class="chat-window">
            <slot></slot>
        </div>
        <form class="chat-form">
            <textarea class="text" cols="30"></textarea>
            <input type="button" class="button" value="send">
        
        </form>
        <button class="reset">Reset</button>
    </div>
`
let loginTemplate = document.createElement('template')
loginTemplate.innerHTML = `
    <style>
    :host {
    margin: 20px;
    }
    div {
    height: 200px;
    }
     
    .setUserName {
    position: center;
    display: block;
    margin: auto;
    }
    .text {
    display: block;
    margin: auto 0;
    
    }
    
</style>
    
    <div>
    <p>Enter Username</p>
        <form>
            <textarea class="text" cols="30"></textarea>
            <input type="button" class="setUserName" value="send">
        
        </form>
    </div>
`
class LnuChat extends window.HTMLElement {
  constructor () {
    super()
    this.attachShadow({mode: 'open'})
    this.userName = null
  }

  connectedCallback () {
    this.getUserName()
    // console.log(this.socket)
    if (this.userName !== null) {
      this.chatGUI()
    } else {
      this.logInGUI()
    }
  }

  logInGUI () {
    this.shadowRoot.innerHTML = ''
    this.shadowRoot.appendChild(loginTemplate.content.cloneNode(true))
    let button = this.shadowRoot.querySelector('.setUserName')
    button.addEventListener('click', e => {
      this.setUserName(this.shadowRoot.querySelector('.text').value)
      this.chatGUI()
    })
  }

  chatGUI () {
    this.socket = new window.WebSocket('ws://vhost3.lnu.se:20080/socket/')      // disconnected callback should disconnect
    this.shadowRoot.innerHTML = ''
    this.shadowRoot.appendChild(BaseTemplate.content.cloneNode(true))
    this.chatText = this.shadowRoot.querySelector('.chat-window')
    this.sendButton = this.shadowRoot.querySelector('.button')
    this.resetButton = this.shadowRoot.querySelector('.reset')
    this.textToSend = this.shadowRoot.querySelector('.text')
    this.socket.addEventListener('message', (e) => {
      this.receiveMessage(e)
    })
    // this.onSendMessageButton = e => this.sendMessage() OM JAG GÖR DETTA SÅ FÅR JAG FEL ANGÅENDE STATE OF WEBSOCKET.
    this.sendButton.addEventListener('click', (e) => {
      this.sendMessage()
      this.textToSend.value = ''
    })

    this.resetButton.addEventListener('click', (e) => {
      this.socket.close()
      this.resetUser()
      this.logInGUI()
    })
  }

  receiveMessage (e) {
    if (this.children.length >= 25) {
      this.removeChild(this.firstChild)
    }
    let type = JSON.parse(e.data).type
    if (type === 'message') {
      let message = JSON.parse(e.data).data
      let sender = JSON.parse(e.data).username
      let dateTime = new Date()
      let date = dateTime.getFullYear() + '-' + (dateTime.getMonth() + 1) + '-' + dateTime.getDate()
      let time = dateTime.getHours() + ':' + dateTime.getMinutes() + ':' + dateTime.getSeconds()
      let timeElement = document.createElement('p')
      let messageText = document.createElement('p')
      let messageElement = document.createElement('div')
      timeElement.textContent = sender + ' @ ' + date + ' ' + time
      messageText.textContent = message
      messageElement.appendChild(timeElement)
      messageElement.appendChild(messageText)
      this.appendChild(messageElement)
    }
  }

  sendMessage () {
    let messageObject =
      {
        type: 'message',
        data: this.textToSend.value,
        username: this.userName,
        channel: 'test',
        key: 'eDBE76deU7L0H9mEBgxUKVR0VCnq0XBd'
      }
    let tmp = JSON.stringify(messageObject)
    this.socket.send(tmp)
  }

  setUserName (userName) {
    this.userName = userName
    let tmpData = {
      UserName: userName
    }
    window.localStorage.setItem('chatUser', JSON.stringify(tmpData))
  }

  getUserName () {
    let tmpUserName = JSON.parse(window.localStorage.getItem('chatUser'))
    if (tmpUserName === null) {
      this.userName = null
    } else {
      this.userName = JSON.parse(window.localStorage.getItem('chatUser')).UserName
    }
  }

  resetUser () {
    window.localStorage.removeItem('chatUser')
  }

  disconnectedCallback () {
    this.socket.close()
  }
}
window.customElements.define('lnu-chat', LnuChat)

module.exports = LnuChat

  // api KEY: eDBE76deU7L0H9mEBgxUKVR0VCnq0XBd
