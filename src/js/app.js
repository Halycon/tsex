require('./app-window')
require('./lnuChat')
require('./memoryApp')
const WindowHandler = require('./windowHandler')

let windowHandler = new WindowHandler()
windowHandler.handleWindow()

let footer = document.querySelector('.bottom')

let lnuChatWindowApp = document.createElement('button')
lnuChatWindowApp.innerHTML = `LNU CHAT!`

let memoryWindowApp = document.createElement('button')
memoryWindowApp.innerHTML = `Memory!`

footer.appendChild(lnuChatWindowApp)
footer.appendChild(memoryWindowApp)

lnuChatWindowApp.addEventListener('click', () => {
  let app = 'lnu-chat'
  windowHandler.newWindow(app)
})

memoryWindowApp.addEventListener('click', () => {
  let app = 'memory-app'
  windowHandler.newWindow(app)
})

/*
let target = document.querySelector('#target')
let testElement = document.createElement('app-window')
testElement.setAttribute('id', '2')

target.appendChild(testElement)

let insert = document.createElement('div')
insert.innerHTML = `<p> hello </p>`
testElement.appendChild(insert)
*/
/*
let targetElement = document.querySelector('#target')
let draggableElement = document.querySelector('#to-drag')
let menuElement = document.querySelector('#menu')
let ox, oy, ax, ay

menuElement.addEventListener('mousedown', () => {
  'use strict'
  console.log('you mousedowned')
  draggableElement.setAttribute('draggable', 'true')
})

menuElement.addEventListener('mouseleave', () => {
  'use strict'
  console.log('you left')
  draggableElement.setAttribute('draggable', 'false')
})

draggableElement.ondragstart = function onDragStartHandler (e) {
  console.log('Ondragstart')
  console.log(e.target.id)
  ox = e.clientX
  oy = e.clientY
  console.log(window.getComputedStyle(draggableElement, null).getPropertyValue('left').slice(0, -2) | 0)
  ax = window.getComputedStyle(draggableElement, null).getPropertyValue('left').slice(0, -2) | 0
  ay = window.getComputedStyle(draggableElement, null).getPropertyValue('top').slice(0, -2) | 0
  console.log(ax)
  console.log(ay)
  e.dataTransfer.setData('text/plain', e.target.id)
  e.dropEffect = 'move'
}

targetElement.ondragover = function onDragOverHandler (e) {
  e.preventDefault()
  e.dataTransfer.dropEffect = 'move'
}

targetElement.ondrop = function onDropHandler (e) {
  e.preventDefault()
  let data = e.dataTransfer.getData('text')
  let testdrop = document.getElementById(data)
  testdrop.style.left = (ax + e.clientX - ox) + 'px'
  testdrop.style.top = (ay + e.clientY - oy) + 'px'
  console.log(ax)
  console.log(ay)
  return false
}
*/
