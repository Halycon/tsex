// gär en klass oc sen instansiera och sen
//

class WindowHandler {
  constructor () {
    this.elementId = 0
    this.maxZIndex = 0
  }

  newWindow (appToWindow = 'lnu-chat') {
    this.elementId++
    let target = document.querySelector('#target')
    let windowElement = document.createElement('app-window')
    let appElement = document.createElement(appToWindow)
    windowElement.setAttribute('id', this.elementId) // Sätt id på fönster
    windowElement.setAttribute('data-target', this.elementId)
    windowElement.appendChild(appElement)
    target.appendChild(windowElement)
    // windowElement.querySelector('.app-icon').setAttribute('src', `image/${appToWindow}.png`)
    this.setZIndex(windowElement)
    console.log(windowElement)
    windowElement.shadowRoot.querySelector('.app-icon').setAttribute('src', `image/${appToWindow}.svg`)
  }

  uppdateWindowList () {
    this.windowlist = document.querySelectorAll('app-window')
  }

  handleWindow () {
    let desktop = document.querySelector('#target')
    desktop.addEventListener('mousedown', (e) => {
      if (e.target.nodeName === 'APP-WINDOW' && parseInt(e.target.style.zIndex) !== this.maxZIndex) {
        this.setZIndex(e.target)
      }
      /* console.log('windowhandler class stuff')
      let zIndexArr = []
      let windowElementList = desktop.querySelectorAll('app-window')
      windowElementList.forEach(element => {
        zIndexArr.push(parseInt(element.style.zIndex))
      })
      zIndexMax = Math.max(...zIndexArr)
      if (e.target.nodeName === 'APP-WINDOW' && parseInt(e.target.style.zIndex) !== zIndexMax) {
        e.target.style.zIndex = ++zIndexMax

        console.log(zIndexMax)
        console.log(e.target.style)
      }

       if (e.target.nodeName === 'APP-WINDOW') {
        console.log(zIndex)
        console.log(parseInt(e.target.style.zIndex))
        console.log(e.target)
        if () {
          e.target.style.zIndex = zIndex++
          e.target.setAttribute('data-target', zIndex)
          let windowElementList = desktop.querySelectorAll('app-window')
          windowElementList.forEach(element => {
            let elementDataTarget = parseInt(element.getAttribute('data-target'))
            if (elementDataTarget < zIndex) {
              element.setAttribute('data-target', elementDataTarget)
            }
          })
        }
      } */
    })
  }

  setZIndex (inputElement) {
    let zIndexArr = [0]
    this.uppdateWindowList()
    this.windowlist.forEach(element => {
      if (element.style.zIndex !== '') {
        zIndexArr.push(parseInt(element.style.zIndex))
      }
    })
    this.maxZIndex = Math.max(...zIndexArr) + 1
    inputElement.style.zIndex = this.maxZIndex
    this.handleDataTarget(inputElement)
  }

  handleDataTarget (inputElement) {
    inputElement.setAttribute('data-target', this.maxZIndex)
    this.uppdateWindowList()
    this.windowlist.forEach(element => {
      if (parseInt(element.getAttribute('data-target')) !== this.maxZIndex) {
        element.setAttribute('data-target', '0')
      }
    })
  }
}

/* function windowHandler () {
  let desktop = document.querySelector('#target')
  let zIndexMax = 0
  desktop.addEventListener('mousedown', (e) => {
    let zIndexArr = []
    let windowElementList = desktop.querySelectorAll('app-window')
    windowElementList.forEach(element => {
      zIndexArr.push(parseInt(element.style.zIndex))
    })
    zIndexMax = Math.max(...zIndexArr)
    if (e.target.nodeName === 'APP-WINDOW' && parseInt(e.target.style.zIndex) !== zIndexMax) {
      e.target.style.zIndex = ++zIndexMax

      console.log(zIndexMax)
      console.log(e.target.style)
    }

  })
} */

module.exports = WindowHandler
