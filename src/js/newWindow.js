
function newWindow (appToWindow = 'lnu-chat') {
  let idArr = [-1]
  let zIndexArr = [0]
  let appWindows = document.querySelectorAll('app-window')
  appWindows.forEach(element => {
    idArr.push(parseInt(element.id))
    zIndexArr.push(parseInt(element.style.zIndex))
  })
  let idNr = (Math.max(...idArr)) + 1
  let maxZIndex = (Math.max(...zIndexArr)) + 1
  let target = document.querySelector('#target')
  let windowElement = document.createElement('app-window')
  let appElement = document.createElement(appToWindow)
  windowElement.setAttribute('id', idNr) // Sätt id på fönster
  windowElement.setAttribute('data-target', idNr)
  windowElement.style.zIndex = maxZIndex
  windowElement.appendChild(appElement)

  target.appendChild(windowElement)
}

module.exports = newWindow
