let BoardTemplate = document.createElement('template')
BoardTemplate.innerHTML = `
    <style>
    :host {
    margin: 20px;
    }
    .select-board {
      background: bisque;  
    }
    .board-window {
      min-height: 205px;
      min-width: 205px;
      margin: 20px;
      background: #c194b7;
    }
    .brick-div {
        display: inline-block;
    }
    
    .brick {
        display: inline-block;
        height: 80px;
        border: 2px solid transparent;
    }
    
    .selected {
        border: 2px solid black;
    }
    
    
</style>
    <div>
        <div class="select-board">
            <select name="select">
              <option value="4">2x2</option>
              <option value="6">2x3</option>
              <option value="12">3x4</option>
              <option value="16">4x4</option>
            </select>
            <button class="button">New Board</button>
            <div class="board-window">
        </div>
        </div>
        
        
    </div>
`
const brickTemplate = document.createElement('template')
brickTemplate.innerHTML = `
  <div class="brick-div">
    <img class="brick" href="#"></img>
  </div>
`
const timeTemplate = document.createElement('template')
timeTemplate.innerHTML = `
<p></p>

`

class MemoryApp extends window.HTMLElement {
  constructor () {
    super()
    this.attachShadow({mode: 'open'})

    this.boardSize = 0
    this.firstSetup = true
  }

  connectedCallback () {
    console.log('MemoryApp Connected callback')
    this.shadowRoot.appendChild(BoardTemplate.content.cloneNode(true))
    this.selectButton = this.shadowRoot.querySelector('.button')
    this.selectedSize = this.shadowRoot.querySelector('select')
    this.playBoard = this.shadowRoot.querySelector('.board-window')
    this.selectButton.addEventListener('click', e => this.generateBoard())
    this.setAttribute('data-target-app', '1')
    this.generateTimeBoard()
    // this.parentElement.shadowRoot.querySelector('.app-icon').setAttribute('src', 'image/memory.png' )
  }

  static get observedAttributes () {
    return ['data-target-app']
  }

  attributeChangedCallback (attrName, oldVal, newVal) {
    if (attrName === 'data-target-app') {
      if (parseInt(newVal) >= 1) {
        this.setKeyboardListener()
        console.log('set listener?')
      } else if (parseInt(newVal) === 0) {
        console.log('remove')
        this.removeKeyboardListener()
      }
    }
  }

  disconnectedCallback () {
    this.removeKeyboardListener()
    this.playBoard.removeEventListener('click', this.toFlipBrick)
    console.log('removed memory app')
  }

  generateTimeBoard () {
    this.playBoard.innerHTML = ``
    this.playBoard.appendChild(timeTemplate.content.cloneNode(true))
    if (this.time > 0) {
      let timeParagraph = this.playBoard.querySelector('p')
      timeParagraph.textContent = this.time + ' heja'
    }
  }

  generateBoard () {
    this.brickArray = []
    this.pairCompare = []
    this.tries = 0
    this.solved = 0
    this.resetTimer = 0
    this.time = 0
    this.firstFlip = true
    // this.playBoard = this.shadowRoot.querySelector('.board-window')
    this.playBoard.innerHTML = ''
    this.boardSize = this.selectedSize.options[this.selectedSize.selectedIndex].value
    this.numberOfRows = Math.ceil(Math.sqrt(this.boardSize))
    this.rowSize = this.boardSize / this.numberOfRows
    let brickIdCounter = 0

    for (let j = 0; j < this.numberOfRows; j++) {
      let rowDiv = document.createElement('div')
      rowDiv.classList.add('rowDiv')
      rowDiv.id = 'row' + j
      for (let i = 0; i < this.boardSize / this.numberOfRows; i++) {
        let newDiv = brickTemplate.content.cloneNode(true)
        let imgTemp = newDiv.querySelector('img')
        imgTemp.setAttribute('src', 'image/0.png')
        imgTemp.id = brickIdCounter++

        rowDiv.appendChild(newDiv)
      }

      this.playBoard.appendChild(rowDiv)
    }
    if (this.firstSetup) {
      this.toFlipBrick = e => this.flipBrick(e)
      this.playBoard.addEventListener('click', this.toFlipBrick)
    }

    this.shadowRoot.getElementById('0').classList.add('selected')
    this.firstSetup = false
    this.rndBrickList()
  }

  setKeyboardListener () {
    this.bodyElement = document.querySelector('body')
    this.useKeyboard = e => this.keyboardControl(e)
    this.bodyElement.addEventListener('keydown', this.useKeyboard, true)
  }

  removeKeyboardListener () {
    this.bodyElement.removeEventListener('keydown', this.useKeyboard, true)
  }

  rndBrickList () {
    let toRandomize = []

    for (let i = 0; i < this.boardSize / 2; i++) {
      toRandomize.push(i + 1)
      toRandomize.push(i + 1)
    }

    let length = toRandomize.length
    let randomIndex = null
    let temp = null

    while (length) {
      randomIndex = Math.floor(Math.random() * length--)

      temp = toRandomize[length]
      toRandomize[length] = toRandomize[randomIndex]
      toRandomize[randomIndex] = temp
    }
    for (let i = 0; i < toRandomize.length; i++) {
      this.brickArray.push({ index: i, element: this.shadowRoot.getElementById(i), card: toRandomize[i], flipped: false, solved: false })
    }
  }

  flipBrick (e) {
    let brickTarget = this.brickArray[e.target.id]
    if (brickTarget.element.style.visibility !== 'hidden' && brickTarget.flipped === false) {
      this.shadowRoot.querySelector('.selected').classList.toggle('selected')
      brickTarget.element.setAttribute('src', `image/${brickTarget.card}.png`)
      brickTarget.flipped = true
      this.checkBricks(brickTarget.index)
      this.setSelected(brickTarget, 'ArrowRight')
      if (this.firstFlip === true) {
        this.gameTimer()
        this.firstFlip = false
      }
    }
  }

  checkBricks (targetID) {
    if (this.pairCompare.length === 2) {
      this.pairCompare[0].element.setAttribute('src', `image/0.png`)
      this.pairCompare[1].element.setAttribute('src', `image/0.png`)
      this.pairCompare[0].flipped = false
      this.pairCompare[1].flipped = false
      this.pairCompare = []
    }
    this.pairCompare.push(this.brickArray[parseInt(targetID)])
    if (this.pairCompare.length === 1) {
      return
    }

    this.tries++
    if (this.pairCompare[0].card === this.pairCompare[1].card) {
      this.pairCompare[0].solved = true
      this.pairCompare[1].solved = true
      this.pairCompare[0].element.style.visibility = 'hidden'
      this.pairCompare[1].element.style.visibility = 'hidden'
      this.solved++
      this.pairCompare = []

      if (this.solved === this.boardSize / 2) {
        clearInterval(this.resetTimer)
        console.log(`You win! With a reasonable number of attempts of: ${this.tries}`)
        this.generateTimeBoard()
      }
    }
  }

  keyboardControl (e) {
    let selectedElement = this.shadowRoot.querySelector('.selected')
    if (e.defaultPrevented) {
      return
    }

    switch (e.key) {
      case 'ArrowDown':
        selectedElement.classList.toggle('selected')
        console.log('down')
        if (parseInt(selectedElement.id) + this.rowSize + 1 > this.brickArray.length) {
          this.setSelected(this.brickArray[parseInt(selectedElement.id) - this.brickArray.length + this.rowSize], e.key)
        } else {
          this.setSelected(this.brickArray[parseInt(selectedElement.id) + this.rowSize], e.key)
        }
        break
      case 'ArrowUp':
        console.log('Up')
        selectedElement.classList.toggle('selected')
        if (parseInt(selectedElement.id) - this.rowSize < 0) {
          this.setSelected(this.brickArray[parseInt(selectedElement.id) + this.brickArray.length - this.rowSize], e.key)
        } else {
          this.setSelected(this.brickArray[parseInt(selectedElement.id) - this.rowSize], e.key)
        }
        break
      case 'ArrowLeft':
        console.log('lef')
        selectedElement.classList.toggle('selected')
        if (parseInt(selectedElement.id) - 1 < 0) {
          this.setSelected(this.brickArray[this.brickArray.length - 1], e.key)
        } else {
          this.setSelected(this.brickArray[parseInt(selectedElement.id) - 1], e.key)
        }
        break

      case 'ArrowRight':
        console.log('right')
        selectedElement.classList.toggle('selected')
        if (parseInt(selectedElement.id) + 1 > this.brickArray.length - 1) {
          this.setSelected(this.brickArray[0], e.key)
        } else {
          this.setSelected(this.brickArray[parseInt(selectedElement.id) + 1], e.key)
        }
        break

      case 'Enter':
        console.log('enter')
        let myEvent = {target: selectedElement}
        this.flipBrick(myEvent)
        break
      default:
        return
    }
    e.preventDefault()
  }
  setSelected (brickArrayObject, eventType) {
    if (this.solved !== this.boardSize / 2) {
      if (brickArrayObject === undefined || brickArrayObject.solved === true) {
        if (eventType === 'ArrowDown' || eventType === 'ArrowRight') {
          if (brickArrayObject.index === this.brickArray.length - 1) {
            this.setSelected(this.brickArray[0], eventType)
          } else {
            this.setSelected(this.brickArray[brickArrayObject.index + 1], eventType)
          }
        } else if (eventType === 'ArrowUp' || eventType === 'ArrowLeft') {
          if (this.brickArray[brickArrayObject.index].index - 1 < 0) {
            this.setSelected(this.brickArray[this.brickArray.length - 1], eventType)
          } else {
            this.setSelected(this.brickArray[brickArrayObject.index - 1], eventType)
          }
        }
      } else {
        brickArrayObject.element.classList.toggle('selected')
      }
    }
  }

  gameTimer () {
    this.resetTimer = setInterval(() => {
      this.time++
      console.log(this.time)
    }, 1000)
  }
}

window.customElements.define('memory-app', MemoryApp)

module.exports = MemoryApp
