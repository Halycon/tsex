const template = document.createElement('template')
template.innerHTML = `
  <style>
      :host {
        position: absolute;
        top: 30px;
        left: 268px;
        background: yellow;
      }
      
      .menu {
        background: cadetblue;
        margin: 0;
        height: 30px;
      }
      .close {
      height: 90%;
      display: inline-block;
      }
      .app-icon {
      display: inline-block;
      float: right;
      height: 90%;
      }
      
      p {
      margin: 0;
      }
      
  </style>
  <div class="menu">
    <img src="image/ic_close_black_48px.svg" class="close">
    <img src="image/ic_close_black_48px.svg" class="app-icon">
  </div>
  <div class="content">
  <p>Content</p>
  </div>
  <slot></slot>
`

class AppWindow extends window.HTMLElement {
  constructor () {
    super()
    this.attachShadow({mode: 'open'})
    this.shadowRoot.appendChild(template.content.cloneNode(true))
  }

  connectedCallback () {
    let handle = this.shadowRoot.querySelector('.menu')
    let close = this.shadowRoot.querySelector('.close')
    let appIcon = this.shadowRoot.querySelector('.app-icon')
    let ox, oy, ax, ay

    this.classList.add('app-window')
    this.setAttribute('draggable', 'false')

    this.ondragstart = function onDragStartHandler (e) {
      ox = e.clientX
      oy = e.clientY
      // Hur ska detta justeras? "style="left; top"-stilen"?
      ax = window.getComputedStyle(this, null).getPropertyValue('left').slice(0, -2) | 0
      ay = window.getComputedStyle(this, null).getPropertyValue('top').slice(0, -2) | 0
      e.dataTransfer.setData('text/plain', e.target.id)
      this.focus()
      e.dropEffect = 'move'
    }

    this.parentElement.ondragover = function onDragOverHandler (e) {
      e.preventDefault()
      e.dataTransfer.dropEffect = 'move'
    }

    this.parentElement.ondrop = function onDropHandler (e) {
      e.preventDefault()
      let data = e.dataTransfer.getData('text')
      let self = document.getElementById(data)
      self.style.left = (ax + e.clientX - ox) + 'px'
      self.style.top = (ay + e.clientY - oy) + 'px'
      return false
    }

    handle.addEventListener('mousedown', () => {
      this.setAttribute('draggable', 'true')
    })

    handle.addEventListener('mouseleave', () => {
      this.setAttribute('draggable', 'false')
    })


    close.addEventListener('click', () => {
      this.remove()
    })
  }
  static get observedAttributes () {
    return ['data-target']
  }

  attributeChangedCallback (attrName, oldVal, newVal) {
    if (attrName === 'data-target') {
      let appdiv = this.firstChild
      if (appdiv !== null && oldVal < newVal) {
        appdiv.setAttribute('data-target-app', newVal)
      } else if (appdiv !== null && parseInt(newVal) === 0) {
        appdiv.setAttribute('data-target-app', newVal)
      }
    }
  }

  disconnectedCallback () {
    this.removeChild(this.firstChild)
  }
}

window.customElements.define('app-window', AppWindow)

module.exports = AppWindow
